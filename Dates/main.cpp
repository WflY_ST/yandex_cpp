#include <iostream>
#include <sstream>
#include <map>
#include <set>
#include <string>
#include <exception>
#include <iomanip>

using namespace std;

class Date {
public:
    Date() {
        day = 01;
        month = 01;
        year = 0;
    }

    Date(int new_year, int new_month, int new_day) {
        if (new_month < 1 || new_month > 12) {
            throw logic_error("Month value is invalid: " + new_month);
        }
        if (new_day < 1 || new_day > 31) {
            throw logic_error("Day value is invalid:" + new_day);
        }
        year = new_year;
        month = new_month;
        day = new_day;
    }

    int GetYear() const {
        return year;
    }

    int GetMonth() const {
        return month;
    }

    int GetDay() const {
        return day;
    }

private:
    int year, month, day;
};

bool operator<(const Date &lhs, const Date &rhs) {
    if (lhs.GetYear() < rhs.GetYear()) {
        return true;
    } else if (lhs.GetMonth() < rhs.GetMonth()) {
        return true;
    } else if (lhs.GetDay() < rhs.GetDay()) {
        return true;
    }
    return false;
}

ostream &operator<<(ostream &stream, const Date &date) {
    stream << setw(4) << setfill('0') << date.GetYear() <<
           "-" << setw(2) << setfill('0') << date.GetMonth() <<
           "-" << setw(2) << setfill('0') << date.GetDay();
    return stream;
}

bool operator==(const Date &date1, const Date &date2) {
    if (date1.GetYear() == date2.GetYear() && date1.GetMonth() == date2.GetMonth() &&
        date1.GetDay() == date2.GetDay()) {
        return true;
    }
    return false;
}

Date ParseDate(const string &s) {
    stringstream stream(s);
    int day, month, year;
    bool ok = true;

    ok = ok && (stream >> year);
    ok = ok && (stream.peek() == '-');

    ok = ok && (stream >> month);
    ok = ok && (stream.peek() == '-');

    ok = ok && (stream >> day);


    Date date = Date(year, month, year);
    if (!ok) {
        throw runtime_error("Wrong date format ");
    } else {
        return date;
    }
}

class Database {
public:
    void AddEvent(const Date &date, const string &event) {
        database[date].insert(event);
    }

    bool DeleteEvent(const Date &date, const string &event) {
        if (database.count(date) > 0 && database[date].count(event) > 0) {
            database[date].erase(event);
            return true;
        } else {
            return false;
        }
    }

    int DeleteDate(const Date &date) {
        if (database.count(date) == 0) {
            return 0;
        } else {
            int save_date = database[date].size();
            database.erase(date);
            return save_date;
        }
    }

    set<string> Find(const Date &date) const {
        for (const auto &item : database) {
            if (item.first == date) {
                for (const string &i: item.second) {
                    cout << date << " " << i << endl;
                }
            }
        }
    }

    void Print() const {
        for (const auto &item : database) {
            for (const string &i : item.second) {
                cout << item.first << " " << i << endl;
            }
        }
    }

private:
    map<Date, set<string>> database;
};

int main() {
    Database db;

    string command;
    while (getline(cin, command)) {
        // Считайте команды с потока ввода и обработайте каждую
        if (command == "Add") {
            string date, event;

            cin >> date >> event;

            Date date_pars = ParseDate(date);
            db.AddEvent(date_pars, event);
        } else if (command == "Del") {
            string date, event;

            cin >> date >> event;

            Date date_pars = ParseDate(date);
            if (event.empty()) {
                db.DeleteDate(date_pars);
            } else {
                db.DeleteEvent(date_pars, event);
            }
        } else if (command == "Find") {
            string date;

            cin >> date;

            Date date_pars = ParseDate(date);
            db.Find(date_pars);
        } else if (command == "Print") {
            db.Print();
        }
    }

    return 0;
}