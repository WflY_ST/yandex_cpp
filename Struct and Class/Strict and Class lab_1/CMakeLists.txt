cmake_minimum_required(VERSION 3.17)
project(Struct_and_Class)

set(CMAKE_CXX_STANDARD 20)

add_executable(Struct_and_Class main.cpp)