#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int main() {
    ifstream input("input.txt");

    int column, row;
    string line;

    input >> column >> row;
    input.ignore(1);

    for (int i = 0; i < column; i++) {
        for (int j = 1; j < row; j++) {
            getline(input, line, ',');
            cout << setw(10) << line << " ";
        }
        getline(input, line);
        cout << setw(10) << line << endl;
    }
    return 0;
}