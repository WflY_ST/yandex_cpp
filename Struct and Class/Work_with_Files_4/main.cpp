#include <iostream>
#include <vector>
#include <string>

using namespace std;

struct Student {
    string Name = "";
    string Surname = "";
    int Day = 0;
    int Month = 0;
    int Year = 0;
};

int main() {
    vector<Student> vector_students{};
    int num_students, num_quest;

    cin >> num_students;

    if (0 <= num_students <= 10000) {
        for (int i = 0; i < num_students; i++) {
            string name, surname;
            int day, month, year;

            cin >> name >> surname >> day >> month >> year;

            if ((1 <= name.length() <= 15) && (1 <= surname.length() <= 15) &&
                (0 <= day <= 1000000000) && (0 <= month <= 1000000000) && (0 <= year <= 1000000000)) {
                vector_students.push_back({name, surname, day, month, year});
            }
        }
    }

    cin >> num_quest;

    if (0 <= num_quest <= 10000) {
        for (int i = 0; i < num_quest; i++) {
            string quest;
            int number_student_for_quest;

            cin >> quest >> number_student_for_quest;

            number_student_for_quest = number_student_for_quest - 1;
            if ((quest == "name") && (1 <= quest.length() <= 15)) {
                if ((0 <= number_student_for_quest) && (number_student_for_quest <= num_students - 1)) {
                    cout << vector_students[number_student_for_quest].Name << " "
                         << vector_students[number_student_for_quest].Surname << endl;
                } else {
                    cout << "bad request" << endl;
                }
            } else if ((quest == "date") && (1 <= quest.length() <= 15)) {
                if ((0 <= number_student_for_quest) && (number_student_for_quest <= num_students - 1)) {
                    cout << vector_students[number_student_for_quest].Day << "."
                         << vector_students[number_student_for_quest].Month << "."
                         << vector_students[number_student_for_quest].Year << endl;
                } else {
                    cout << "bad request" << endl;
                }
            } else {
                cout << "bad request" << endl;
            }
        }
    }

    return 0;
}