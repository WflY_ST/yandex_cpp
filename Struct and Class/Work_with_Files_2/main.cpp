#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

int main() {
    ifstream input("/Struct and Class/Work_with_Files_2/input.txt");

    if (input) {
        double line;

        cout << fixed << cout.precision(3);
        while (input >> line) {
            cout << line << endl;
        }
    } else {
        cout << "error";
    }

    return 0;
}