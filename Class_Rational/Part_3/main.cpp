#include <iostream>
#include <numeric>

using namespace std;

class Rational {
public:


    Rational() { //конструктор по умолчанию
        num = 0;
        den = 1;
    }
    Rational(int numerator, int denominator) {
        if (denominator < 0) {
            if (numerator < 0) {
                denominator = abs(denominator);
                numerator = abs(numerator);
            }
            else {
                denominator = abs(denominator);
                numerator = -abs(numerator);
            }
        }
        if (numerator == 0) {
            denominator = 1;
        }
        NOD = gcd(abs(numerator), abs(denominator));
        num = numerator / NOD;
        den = denominator / NOD;
    }
    int Numerator() const {
        return num;
    }
    int Denominator() const {
        return den;
    }
private:
    int num;
    int den;
    int NOD;
};

// Реализуйте для класса Rational операторы ==, + и -

bool operator==(Rational r1, Rational r2) {
    if (r1.Numerator() == r2.Numerator() && r1.Denominator() == r2.Denominator()) {
        return 1;
    } else {
        return 0;
    }
}

// 2/3 + 5/6 = (12+15) /18 = 27/18 = 3/2
Rational operator+(Rational r1, Rational r2) {
    int a = (r1.Numerator() * r2.Denominator()) + (r2.Numerator() * r1.Denominator());
    int b = r1.Denominator() * r2.Denominator();
    return Rational(a, b);
}

Rational operator-(Rational r1, Rational r2) {
    int a = (r1.Numerator() * r2.Denominator()) - (r2.Numerator() * r1.Denominator());
    int b = r1.Denominator() * r2.Denominator();
    return Rational(a, b);
}

// Реализуйте для класса Rational операторы * и /
Rational operator*(Rational r1, Rational r2) {
    return Rational((r1.Numerator() * r2.Numerator()),
                    (r1.Denominator() * r2.Denominator()));
}

//     1/3  /  1/10 = 10/3
Rational operator/(Rational r1, Rational r2) {
    return Rational((r1.Numerator() * r2.Denominator()),
                    (r1.Denominator() * r2.Numerator()));
}

int main() {
    {
        Rational a(2, 3);
        Rational b(1, 3);
        Rational c = a * b;
        bool equal = c == Rational(2, 9);
        if (!equal) {
            cout << "2/3 * 1/3 != 2/9" << endl;
            return 1;
        }
    }

    {
        Rational a(2, 3);
        Rational b(1, 3);
        Rational c = a / b;
        bool equal = c == Rational(2, 1);
        if (!equal) {
            cout << "2/3 / 1/3 != 2/1" << endl;
            return 2;
        }
    }

    const Rational r = Rational(1, 2) * Rational(1, 3) / Rational(1, 4);
    if (r == Rational(2, 3)) {
        cout << "equal";
    }

    cout << "OK" << endl;
    return 0;
}
