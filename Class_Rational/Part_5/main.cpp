#include <iostream>
#include <numeric>
#include <sstream>
#include <set>
#include <map>
#include <vector>

using namespace std;

class Rational {
public:


    Rational() { //конструктор по умолчанию
        num = 0;
        den = 1;
    }

    Rational(int numerator, int denominator) {
        if (denominator < 0) {
            if (numerator < 0) {
                denominator = abs(denominator);
                numerator = abs(numerator);
            } else {
                denominator = abs(denominator);
                numerator = -abs(numerator);
            }
        }
        if (numerator == 0) {
            denominator = 1;
        }
        NOD = gcd(abs(numerator), abs(denominator));
        num = numerator / NOD;
        den = denominator / NOD;
    }

    int Numerator() const {
        return num;
    }

    int Denominator() const {
        return den;
    }

private:
    int num;
    int den;
    int NOD;
};

// Реализуйте для класса Rational операторы ==, + и -

bool operator==(Rational r1, Rational r2) {
    if (r1.Numerator() == r2.Numerator() && r1.Denominator() == r2.Denominator()) {
        return 1;
    } else {
        return 0;
    }
}

// 2/3 + 5/6 = (12+15) /18 = 27/18 = 3/2
Rational operator+(Rational r1, Rational r2) {
    int a = (r1.Numerator() * r2.Denominator()) + (r2.Numerator() * r1.Denominator());
    int b = r1.Denominator() * r2.Denominator();
    return Rational(a, b);
}

Rational operator-(Rational r1, Rational r2) {
    int a = (r1.Numerator() * r2.Denominator()) - (r2.Numerator() * r1.Denominator());
    int b = r1.Denominator() * r2.Denominator();
    return Rational(a, b);
}

// Реализуйте для класса Rational операторы * и /
Rational operator*(Rational r1, Rational r2) {
    return Rational((r1.Numerator() * r2.Numerator()),
                    (r1.Denominator() * r2.Denominator()));
}

//     1/3  /  1/10 = 10/3
Rational operator/(Rational r1, Rational r2) {
    return Rational((r1.Numerator() * r2.Denominator()),
                    (r1.Denominator() * r2.Numerator()));
}

// Реализуйте для класса Rational операторы << и >>

ostream &operator<<(ostream &stream, const Rational r1) {
    stream << r1.Numerator() << "/" << r1.Denominator();
    return stream;
}

istream &operator>>(istream &stream, Rational &r2) {
    int numerator, denominator;
    char slash;
    if (stream >> numerator && stream >> slash && stream >> denominator) {
        if (slash == '/') {
            r2 = Rational(numerator, denominator);
        }
    }
    return stream;
}

bool operator<(const Rational &r1, const Rational &r2) {
    double a = r1.Denominator() * r2.Denominator();
    double b1 = r1.Numerator() * r2.Denominator();
    double b2 = r2.Numerator() * r1.Denominator();
    double t1 = b1 / a;
    double t2 = b2 / a;
    return t1 < t2;
}

int main() {
    {
        const set<Rational> rs = {{1, 2},
                                  {1, 25},
                                  {3, 4},
                                  {3, 4},
                                  {1, 2}};
        if (rs.size() != 3) {
            cout << "Wrong amount of items in the set" << endl;
            return 1;
        }

        vector<Rational> v;
        for (auto x : rs) {
            v.push_back(x);
        }
        if (v != vector<Rational>{{1, 25},
                                  {1, 2},
                                  {3, 4}}) {
            cout << "Rationals comparison works incorrectly" << endl;
            return 2;
        }
    }

    {
        map<Rational, int> count;
        ++count[{1, 2}];
        ++count[{1, 2}];

        ++count[{2, 3}];

        if (count.size() != 2) {
            cout << "Wrong amount of items in the map" << endl;
            return 3;
        }
    }

    cout << "OK" << endl;
    return 0;
}