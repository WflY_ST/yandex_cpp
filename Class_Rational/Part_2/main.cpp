#include <iostream>
#include <numeric>

using namespace std;

class Rational {
public:
    Rational() {
        numerator_ = 0;
        denominator_ = 1;
    }

    Rational(int numerator, int denominator) {
        numerator_ = abs(numerator);
        denominator_ = abs(denominator);
        NOD = gcd(numerator, denominator);

        if ((numerator * denominator) < 0) {
            numerator_ = -abs(numerator);
            denominator_ = abs(denominator);
        }
    }

    int Numerator() const {
        if (numerator_ == 0) {
            return 0;
        } else {
            return numerator_ / NOD;
        }
    }

    int Denominator() const {
        if (denominator_ == 1) {
            return 1;
        } else {
            return denominator_ / NOD;
        }
    }

private:
    int numerator_;
    int denominator_;
    int NOD;
};

// Реализуйте для класса Rational операторы ==, + и -

bool operator==(Rational r1, Rational r2) {
    if (r1.Numerator() == r2.Numerator() && r1.Denominator() == r2.Denominator()) {
        return 1;
    } else {
        return 0;
    }
}

// 2/3 + 5/6 = (12+15) /18 = 27/18 = 3/2
Rational operator+(Rational r1, Rational r2) {
    int a = (r1.Numerator() * r2.Denominator()) + (r2.Numerator() * r1.Denominator());
    int b = r1.Denominator() * r2.Denominator();
    return Rational(a, b);
}

Rational operator-(Rational r1, Rational r2) {
    int a = (r1.Numerator() * r2.Denominator()) - (r2.Numerator() * r1.Denominator());
    int b = r1.Denominator() * r2.Denominator();
    return Rational(a, b);
}

int main() {
    {
        Rational r1(4, 6);
        Rational r2(2, 3);
        bool equal = r1 == r2;
        if (!equal) {
            cout << "4/6 != 2/3" << endl;
            return 1;
        }
    }

    {
        Rational a(2, 3);
        Rational b(4, 3);
        Rational c = a + b;
        bool equal = c == Rational(2, 1);
        if (!equal) {
            cout << "2/3 + 4/3 != 2" << endl;
            return 2;
        }
    }

    {
        Rational a(5, 7);
        Rational b(2, 9);
        Rational c = a - b;
        bool equal = c == Rational(31, 63);
        if (!equal) {
            cout << "5/7 - 2/9 != 31/63" << endl;
            return 3;
        }
    }

    cout << "OK" << endl;
    return 0;
}