#include <iostream>
#include <numeric>
#include <sstream>

using namespace std;

class Rational {
public:


    Rational() { //конструктор по умолчанию
        num = 0;
        den = 1;
    }

    Rational(int numerator, int denominator) {
        if (denominator < 0) {
            if (numerator < 0) {
                denominator = abs(denominator);
                numerator = abs(numerator);
            } else {
                denominator = abs(denominator);
                numerator = -abs(numerator);
            }
        }
        if (numerator == 0) {
            denominator = 1;
        }
        NOD = gcd(abs(numerator), abs(denominator));
        num = numerator / NOD;
        den = denominator / NOD;
    }

    int Numerator() const {
        return num;
    }

    int Denominator() const {
        return den;
    }

private:
    int num;
    int den;
    int NOD;
};

// Реализуйте для класса Rational операторы ==, + и -

bool operator==(Rational r1, Rational r2) {
    if (r1.Numerator() == r2.Numerator() && r1.Denominator() == r2.Denominator()) {
        return 1;
    } else {
        return 0;
    }
}

// 2/3 + 5/6 = (12+15) /18 = 27/18 = 3/2
Rational operator+(Rational r1, Rational r2) {
    int a = (r1.Numerator() * r2.Denominator()) + (r2.Numerator() * r1.Denominator());
    int b = r1.Denominator() * r2.Denominator();
    return Rational(a, b);
}

Rational operator-(Rational r1, Rational r2) {
    int a = (r1.Numerator() * r2.Denominator()) - (r2.Numerator() * r1.Denominator());
    int b = r1.Denominator() * r2.Denominator();
    return Rational(a, b);
}

// Реализуйте для класса Rational операторы * и /
Rational operator*(Rational r1, Rational r2) {
    return Rational((r1.Numerator() * r2.Numerator()),
                    (r1.Denominator() * r2.Denominator()));
}

//     1/3  /  1/10 = 10/3
Rational operator/(Rational r1, Rational r2) {
    return Rational((r1.Numerator() * r2.Denominator()),
                    (r1.Denominator() * r2.Numerator()));
}

// Реализуйте для класса Rational операторы << и >>

ostream &operator<<(ostream &stream, const Rational r1) {
    stream << r1.Numerator() << "/" << r1.Denominator();
    return stream;
}

istream &operator>>(istream &stream, Rational &r2) {
    int numerator, denominator;
    char slash;
    if (stream >> numerator && stream >> slash && stream >> denominator) {
        if (slash == '/') {
            r2 = Rational(numerator, denominator);
        }
    }
    return stream;
}

int main() {
    {
        ostringstream output;
        output << Rational(-6, 8);
        if (output.str() != "-3/4") {
            cout << "Rational(-6, 8) should be written as \"-3/4\"" << endl;
            return 1;
        }
    }

    {
        istringstream input("5/7");
        Rational r;
        input >> r;
        bool equal = r == Rational(5, 7);
        if (!equal) {
            cout << "5/7 is incorrectly read as " << r << endl;
            return 2;
        }
    }

    {
        istringstream input("");
        Rational r;
        bool correct = !(input >> r);
        if (!correct) {
            cout << "Read from empty stream works incorrectly" << endl;
            return 3;
        }
    }

    {
        istringstream input("5/7 10/8");
        Rational r1, r2;
        input >> r1 >> r2;
        bool correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
        if (!correct) {
            cout << "Multiple values are read incorrectly: " << r1 << " " << r2 << endl;
            return 4;
        }

        input >> r1;
        input >> r2;
        correct = r1 == Rational(5, 7) && r2 == Rational(5, 4);
        if (!correct) {
            cout << "Read from empty stream shouldn't change arguments: " << r1 << " " << r2 << endl;
            return 5;
        }
    }

    {
        istringstream input1("1*2"), input2("1/"), input3("/4");
        Rational r1, r2, r3;
        input1 >> r1;
        input2 >> r2;
        input3 >> r3;
        bool correct = r1 == Rational() && r2 == Rational() && r3 == Rational();
        if (!correct) {
            cout << "Reading of incorrectly formatted rationals shouldn't change arguments: "
                 << r1 << " " << r2 << " " << r3 << endl;

            return 6;
        }
    }

    cout << "OK" << endl;
    return 0;
}