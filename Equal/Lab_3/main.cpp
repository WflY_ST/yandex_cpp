#include <iostream>
#include <numeric>
#include <sstream>
#include <exception>

using namespace std;

class Rational {
public:


    Rational() { //конструктор по умолчанию
        num = 0;
        den = 1;
    }

    Rational(int numerator, int denominator) {
        if (denominator < 0) {
            if (numerator < 0) {
                denominator = abs(denominator);
                numerator = abs(numerator);
            } else {
                denominator = abs(denominator);
                numerator = -abs(numerator);
            }
        } else if (denominator == 0) {
            throw runtime_error("Invalid argument");
        }
        if (numerator == 0) {
            denominator = 1;
        }
        NOD = gcd(abs(numerator), abs(denominator));
        num = numerator / NOD;
        den = denominator / NOD;
    }

    int Numerator() const {
        return num;
    }

    int Denominator() const {
        return den;
    }

private:
    int num;
    int den;
    int NOD;
};

// Реализуйте для класса Rational операторы ==, + и -

bool operator==(Rational r1, Rational r2) {
    if (r1.Numerator() == r2.Numerator() && r1.Denominator() == r2.Denominator()) {
        return 1;
    } else {
        return 0;
    }
}

// 2/3 + 5/6 = (12+15) /18 = 27/18 = 3/2
Rational operator+(Rational r1, Rational r2) {
    int a = (r1.Numerator() * r2.Denominator()) + (r2.Numerator() * r1.Denominator());
    int b = r1.Denominator() * r2.Denominator();
    return Rational(a, b);
}

Rational operator-(Rational r1, Rational r2) {
    int a = (r1.Numerator() * r2.Denominator()) - (r2.Numerator() * r1.Denominator());
    int b = r1.Denominator() * r2.Denominator();
    return Rational(a, b);
}

// Реализуйте для класса Rational операторы * и /
Rational operator*(Rational r1, Rational r2) {
    return Rational((r1.Numerator() * r2.Numerator()),
                    (r1.Denominator() * r2.Denominator()));
}

//     1/3  /  1/10 = 10/3
Rational operator/(Rational r1, Rational r2) {
    if ((r1.Denominator() * r2.Numerator()) == 0) {
        throw runtime_error("Division by zero");
    } else {
        return Rational((r1.Numerator() * r2.Denominator()),
                        (r1.Denominator() * r2.Numerator()));
    }
}

ostream &operator<<(ostream &stream, const Rational r1) {
    stream << r1.Numerator() << "/" << r1.Denominator();
    return stream;
}

istream &operator>>(istream &stream, Rational &r2) {
    int numerator, denominator;
    char slash;
    if (stream >> numerator && stream >> slash && stream >> denominator) {
        if (slash == '/') {
            r2 = Rational(numerator, denominator);
        }
    }
    return stream;
}

int main() {
    try {
        Rational r1, r2;
        char operation;

        cin >> r1 >> operation >> r2;

        if (operation == '+') {
            Rational add = r1 + r2;
            cout<< add;
        } else if (operation == '-') {
            Rational sub = r1 - r2;
            cout<< sub;
        } else if (operation == '*') {
            Rational mult = r1 * r2;
            cout<< mult;
        } else if (operation == '/') {
            Rational div = r1 / r2;
            cout<< div;
        }

    } catch (const runtime_error &in) {
        cout << in.what() << endl;
    }
    return 0;
}
