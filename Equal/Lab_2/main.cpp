#include <iostream>
#include <exception>
#include <numeric>

using namespace std;

class Rational {
public:

    Rational() { //конструктор по умолчанию
        num = 0;
        den = 1;
    }

    Rational(int numerator, int denominator) {
        if (denominator < 0) {
            if (numerator < 0) {
                denominator = abs(denominator);
                numerator = abs(numerator);
            } else {
                denominator = abs(denominator);
                numerator = -abs(numerator);
            }
        } else if (denominator == 0) {
            throw invalid_argument("");
        }
        if (numerator == 0) {
            denominator = 1;
        }
        NOD = gcd(abs(numerator), abs(denominator));
        num = numerator / NOD;
        den = denominator / NOD;
    }

    int Numerator() const {
        return num;
    }

    int Denominator() const {
        return den;
    }

private:
    int num;
    int den;
    int NOD;
};

//     1/3  /  1/10 = 10/3
Rational operator/(Rational r1, Rational r2) {
    if ((r1.Denominator() * r2.Numerator()) == 0) {
        throw domain_error("");
    } else {
        return Rational((r1.Numerator() * r2.Denominator()),
                        (r1.Denominator() * r2.Numerator()));
    }
}

int main() {
    try {
        Rational r(1, 0);
        cout << "Doesn't throw in case of zero denominator" << endl;
        return 1;
    } catch (invalid_argument &) {
    }

    try {
        auto x = Rational(1, 2) / Rational(0, 1);
        cout << "Doesn't throw in case of division by zero" << endl;
        return 2;
    } catch (domain_error &) {
    }

    cout << "OK" << endl;
    return 0;
}